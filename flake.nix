{
  inputs = { nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09"; };

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      splitengineEnv = pkgs.poetry2nix.mkPoetryEnv {
        projectDir = ./.;
        editablePackageSources = { splitengine = ./splitengine; };
      };
    in {
      devShell.x86_64-linux =
        pkgs.mkShell { buildInputs = [ pkgs.poetry ]; };
    };
}
