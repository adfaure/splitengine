#!/usr/bin/env python3

import copy
import datetime
import logging
import os
import random
import subprocess
import sys
import time
from pathlib import Path

import click
import yaml

from .deployment import configure_network, setup_g5k_hosts
from .experiment import Experiment


def save_status(status, file):
    with open(file, "w") as outfile:
        yaml.safe_dump(status, outfile, default_flow_style=False)


@click.command()
@click.option(
    "--keys-folder",
    default=None,
    help="Folder containing the ssh key for the experiment.",
)
@click.argument("filename")
def main(keys_folder, filename):
    logging.basicConfig(format="[%(asctime)s]::%(message)s", level=logging.INFO)
    logging.info("Starting experiment")

    save_file = "exp.{}.yaml".format(int(time.time()))
    status = dict()
    status["start_time"] = datetime.datetime.now()

    if keys_folder is None:
        keys_folder = "/tmp/experiment_keys"
        logging.info("no key folder provided, using default: {}".format(keys_folder))
    else:
        logging.info("Looking for ssh keys in {}".format(keys_folder))

    # Security option for ssh configuration
    os.chmod(keys_folder + "/id_rsa", 0o400)
    os.chmod(keys_folder + "/id_rsa.pub", 0o600)

    experiment_file = filename
    with open(experiment_file) as f:
        # use safe_load instead load
        experiment_conf = yaml.safe_load(f)
        print(experiment_conf)

    all_instances = []
    for instance in experiment_conf:
        filepath = os.path.join(os.path.dirname(experiment_file), instance["file"])
        if not os.path.exists(filepath):
            logging.critical("instance file %s does not exist" % filepath)
            sys.exit()

        instance["file"] = filepath

        for repeat in range(instance["repeat"]):
            all_instances.append(copy.deepcopy(instance))

    random.Random(42).shuffle(all_instances)
    status["all_instances"] = all_instances

    job_id = os.environ["OAR_JOB_ID"]

    # Get the node list from oar environment variables
    nodes_list = subprocess.getoutput("cat $OAR_NODE_FILE|uniq").split("\n")

    setup_g5k_hosts(nodes_list, job_id, keys_folder)
    networks = configure_network(nodes_list, nodes_list)

    save_status(status, save_file)

    logging.info("Isolate net group 1")
    networks[0].isolate_network()
    logging.info("Isolate net group 2")
    networks[1].isolate_network()
    try:
        for instance in all_instances:
            if not instance["groupname"] in status:
                status[instance["groupname"]] = dict()
                group = status[instance["groupname"]]

            save_status(status, save_file)
            local_folder = os.path.join(
                "/home/afaure/batmet_experiments", instance["groupname"]
            )

            Path("local_folder").mkdir(parents=True, exist_ok=True)

            exp = Experiment(
                instance["file"], networks[0], networks[1], root_folder=local_folder
            )
            logging.info("Start instance: {}".format(exp.run_name))

            if not exp.in_data["instance_name"] in group:
                group[exp.in_data["instance_name"]] = dict()

            group[exp.in_data["instance_name"]][exp.run_name] = "start"
            save_status(status, save_file)
            exp.run_instance()
            group[exp.in_data["instance_name"]][exp.run_name] = "finished"
            save_status(status, save_file)
    except Exception as e:
        print("Something bad happended: {}".format(e))
        networks[0].expose_network()
        networks[0].expose_network()

    networks[0].expose_network()
    networks[1].expose_network()

    status["end_time"] = datetime.datetime.now()
    save_status(status, save_file)


if __name__ == "__main__":
    main()
