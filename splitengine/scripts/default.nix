{ hostPkgs ? import (fetchTarball {
  name = "pkgs";
  url = "https://github.com/NixOS/nixpkgs/archive/20.09.tar.gz";
  sha256 = "sha256:1wg61h4gndm3vcprdcg7rc4s1v3jkm5xd7lw8r2f67w502y94gcy";
}) { },
# Last working: f70b1149a9ae30fafe87af46e517985bacc331c3
kapack ? import (fetchTarball {
  name = "kapack";
  url = "https://github.com/oar-team/kapack/archive/master.tar.gz";
  sha256 = "sha256:007xyvrcfl9qfi7947kv3clbb7wjniicn1ks6wf2dlcpd2qzmh22";
}) { } }:
with kapack;
let
  environments = rec {
    inherit kapack pkgs;

    gemmpi = pkgs.stdenv.mkDerivation rec {
      name = "gemmpi-${version}";
      version = "dev";

      src = pkgs.fetchgit {
        url = "https://gitlab.inria.fr/adfaure/gemmpi";
        rev = "f207addeed2c09b25e4798272856a95717cdec53";
        sha256 = "sha256-/VWcNQrdCv7m4PtTdDB5Bu3wsLVtN2FL5nOGrt5L4h4=";
      };

      nativeBuildInputs = with pkgs; [ clang openblas ninja ];

      buildInputs = with pkgs; [ kapack.openmpi simgrid pkgs.boost ];

      buildPhase = ''
        mpicc --version
        mpirun --version
        ninja
      '';

      installPhase = ''
        mkdir -p $out/bin
        cp -r gemmpi   $out/bin/
        cp -r igemmpi   $out/bin/
        cp -r gemsmpi  $out/bin/
        cp -r senrec.mpi $out/bin/
        cp -r pdgemm_stress $out/bin/
        # cp -r gemsmpi_tcpkali $out/bin/
      '';

      meta = with pkgs.stdenv.lib; {
        longDescription = ''
          Performs distributed pdgemm, the algorithm is the direct apadtation of the outer product
          discribed in Parallel Algorithms (Chapman & Hall/CRC Numerical Analysis and Scientific Computing Series).
        '';
        description = ''
          Matrix multiplication benchmark on MPI.
        '';
        homepage = "https://gitlab.inria.fr/adfaure/gemmpi";
        license = licenses.gpl3;
        platforms = platforms.unix;
        broken = false;
      };

    };

    interference_host_env = with pkgs;
      let
        pythonPackages = pkgs.python37Packages;
        python = pkgs.python37;
        periods = import (fetchTarball
          "https://gitlab.inria.fr/adfaure/periods/-/archive/master/periods-master.tar.gz")
          { };

      in pkgs.buildEnv {
        name = "interference_g5k";

        paths = with pkgs; [
          periods.periods
          kapack.openmpi
          gemmpi
          openblas
          mojitos
          (python3.withPackages (ps: with ps; with python3Packages; [ docopt ]))
        ];

        # propagatedBuildInputs = with pythonPackages; [ periods.periods ];

        meta = with stdenv.lib; {
          description = "";
          longDescription = ''
            Experiment environment for the interference experiment on g5k.
          '';
          homepage = "https://gitlab.inria.fr/adfaure/batmet";
          license = licenses.gpl3;
          platforms = platforms.unix;
          broken = false;
        };
      };
  };
in environments

